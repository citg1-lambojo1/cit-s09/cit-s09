package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling http request.
@RestController
//will require all routes within the class to use the set endpoint as part of its route
@RequestMapping("/greeting")
public class DiscussionApplication {
//	private ArrayList<Student> students = new ArrayList<String>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	//localhost:8080/hello
	@GetMapping("/hello")
	//Maps a get request to the route "/hello" and invokes the method hello().
	public String hello(){
		return "Hello World";
	}
	// Route with String Query
	//localhost:8080/hi?name=value
	@GetMapping("/hi")
	//"@RequestParam" annotation that allow us to extract data from query string in URL
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

	//localhost
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}
	
	@GetMapping("/hello/{name}")
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you, %s.", name);

	}

	//S09 - Async Activity
	//1. Create route /welcome ...
	@GetMapping("/welcome")
	public String welcome(@RequestParam(value="user", defaultValue = "Lambojo") String user, @RequestParam(value = "role", defaultValue = "student") String role) {
		switch (role) {
			case "admin":
				return String.format("Welcome back to the class portal, Admin %s!", user);
			case "teacher":
				return String.format("Thank you for logging in, Teacher %s!", user);
			case "student":
				return String.format("Welcome to the class portal, %s!", user);
			default:
				return String.format("Role out of range!");
		}

	}
	//2. create a /register route ...
	private ArrayList<Student> students = new ArrayList<>();
	@GetMapping("/register")
	public String register(@RequestParam(value="id", defaultValue = "123456789") String id, @RequestParam(value = "name", defaultValue = "Jane") String name, @RequestParam(value = "course", defaultValue = "BSIT") String course){
		Student student = new Student(name, id, course);
		students.add(student);
		return String.format("%s your ID number is registered on the system!", id);
	}

	private class Student {
		private String name;
		private String id;
		private String course;

		public Student(String name, String id, String course) {
			this.name = name;
			this.id = id;
			this.course = course;
		}
		public String getName() {
			return name;
		}

		public String getId() {
			return id;
		}

		public String getCourse() {
			return course;
		}
	}

	//3. create a /account/id with path variable
	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") String id) {
		for (Student student : students) {
			if (student.getId().equals(id)) {
				return "Welcome back " + student.getName() + "! You are currently enrolled in " + student.getCourse() + ".";
			}
		}
		return "Your provided " + id + " is not found on the system!";
	}

	//Activity S09
	// 1. Create an arraylist called enrollees
	private ArrayList<String> enrollee = new ArrayList<>();

	//2. Create a //enroll route that will accept a query string with parameter of user
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="user", defaultValue = "Lambojo") String user){
		enrollee.add(user);
		return String.format("Thank you for enrolling, %s!", user);
	}


	//3. Create a new /getEnrollees route which will return the content of the enrollees Arraylist as a string
	@GetMapping("/getEnrollees")
	public String getEnrollees(){
		return enrollee.toString();
	}

	//4. Create a /nameage route that will accept multiple query string parameters of name and age
	@GetMapping("/nameage")
	public String nameage(@RequestParam(value="name", defaultValue = "Lambojo") String name, @RequestParam(value = "age", defaultValue = "21") String age){
		return String.format("Hello %s! My age is %s.", name, age);
	}

	//5. Create a /course/id dynamic route using a path variable of id
	@GetMapping("/course/{id}")
	public String courses(@PathVariable("id") String id){
		String name, sched, price;

		switch (id) {
			case "java101":
				name = "Java 101";
				sched = "MWF 8:00AM-11:00PM";
				price = "PHP 3000";
				return String.format("Name: " + name +", " + "Schedule: "+ sched +", "+ "Price: " + price);
			case "sql101":
				name = "SQL 101";
				sched = "TTH 1:00PM-4:00PM";
				price = "PHP 2000";
				return String.format("Name: " + name +", " + "Schedule: "+ sched +", "+ "Price: " + price);
			case "javaee101":
				name = "Java EE 101";
				sched = "MWF 1:00PM-4:00PM";
				price = "PHP 3500";
				return String.format("Name: " + name +", " + "Schedule: "+ sched +", "+ "Price: " + price);
			default:
				return String.format("Course cannot be found.");
		}

	}

}
